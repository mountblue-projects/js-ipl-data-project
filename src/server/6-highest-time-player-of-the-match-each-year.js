function highestPlayerPerYear(matchesArray) {
    let yearlyMatchData = {};
    let ans = {};

    for (const match of matchesArray) {
        const season = match["season"];
        const playerOfMatch = match["player_of_match"];
        if (yearlyMatchData[season] !== undefined && yearlyMatchData[season][playerOfMatch] == undefined) {
            yearlyMatchData[season][playerOfMatch] = 0;
        }
        if (yearlyMatchData[season] == undefined) {
            yearlyMatchData[season] = {};
            yearlyMatchData[season][playerOfMatch] = 0;
        }
        // else {
        yearlyMatchData[season][playerOfMatch] += 1;
        // }
    }

    for (const year in yearlyMatchData) {
        let highestTimePOM = 0;
        ans[year] = {};
        for (const player in yearlyMatchData[year]) {
            if (yearlyMatchData[year][player] > highestTimePOM) {
                highestTimePOM = yearlyMatchData[year][player];
            }
        }
        for (const player in yearlyMatchData[year]) {
            if (yearlyMatchData[year][player] == highestTimePOM) {
                ans[year][player] = highestTimePOM;
            }
        }
    }
    return ans;
}

// console.log(highestPlayerPerYear(matchesArray));

module.exports = highestPlayerPerYear;