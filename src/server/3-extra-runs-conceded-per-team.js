function extraRunsConceivedPerTeam(matchesArray, deliveriesArray, givenYear) {
    let result = {};

    for (const matches of matchesArray) {
        // filter according to given Year.
        if (matches["season"] == givenYear) {

            let matchId = matches["id"];

            for (const deliveries of deliveriesArray) {
                if (deliveries["match_id"] === matchId) {
                    // counting extra_runs conceived by team;
                    if (result[deliveries["bowling_team"]] == undefined) {
                        result[deliveries["bowling_team"]] = Number(deliveries["extra_runs"]);
                    }
                    else {
                        result[deliveries["bowling_team"]] += Number(deliveries["extra_runs"]);
                    }
                }
            }
        }
    }
    return result;
}

module.exports = extraRunsConceivedPerTeam;