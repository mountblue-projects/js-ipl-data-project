
function numberOfTimesTeamWonTossAndMatch(matchesArray) {
    let teamData = {};
    for (const match of matchesArray) {
        if (match["winner"] == match["toss_winner"]) {
            if (teamData[match.winner] == undefined) {
                teamData[match.winner] = 0;
            }
            teamData[match.winner] += 1;
        }
    }
    return teamData;
}

module.exports = numberOfTimesTeamWonTossAndMatch;