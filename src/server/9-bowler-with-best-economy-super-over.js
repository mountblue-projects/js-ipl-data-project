
function bowlerWithBestEconomy(deliveriesArray) {
    let bowlers = {};

    for (const deliveries of deliveriesArray) {
        const currentBowler = deliveries.bowler;
        if (deliveries["is_super_over"] !== '0') {
            if (bowlers[currentBowler] == undefined) {
                bowlers[currentBowler] = {
                    runs: 0,
                    bowls: 0,
                    economy: 0
                };
            }
            bowlers[currentBowler]["runs"] += Number(deliveries["total_runs"]);
            bowlers[currentBowler]["bowls"] += 1;
            bowlers[currentBowler]["economy"] = bowlers[currentBowler]["runs"] / bowlers[currentBowler]["bowls"];
        }
    }
    let lowestEconomy = 100000;

    for (const bowler in bowlers) {
        if (lowestEconomy > bowlers[bowler]["economy"]) {
            lowestEconomy = bowlers[bowler]["economy"];
        }
    }

    let finalAns = [];

    for (const bowler in bowlers) {
        if (bowlers[bowler]["economy"] == lowestEconomy) {
            finalAns.push(bowler);
        }
    }
    return finalAns;

}

module.exports = bowlerWithBestEconomy;