
function topEconomicalBowlers(matchesArray, deliveriesArray, givenYear, top) {
    let bowlersData = {};

    // creating bowlersData object with his runs balls and economy
    for (const matches of matchesArray) {
        // filtering matches according to the given year.
        if (matches["season"] == givenYear) {
            let matchId = matches["id"];

            for (const deliveries of deliveriesArray) {
                const currentBowler = deliveries.bowler;
                if (deliveries["match_id"] == matchId) {
                    if (bowlersData[currentBowler] == undefined) {
                        bowlersData[currentBowler] = {
                            runs: 0,
                            bowls: 0,
                            economy: 0,
                        }
                    }
                    bowlersData[currentBowler]["runs"] += Number(deliveries["total_runs"]);
                    bowlersData[currentBowler]["bowls"] += 1;
                    bowlersData[currentBowler]["economy"] = bowlersData[currentBowler]["runs"] / bowlersData[currentBowler]["bowls"];
                }
            }
        }
    }

    let bowlersAndEconomyArray = [];

    // creating an array with bowlers name and economy
    for (const bowler in bowlersData) {
        let currentBowlerArray = [bowler, bowlersData[bowler]["economy"]];

        bowlersAndEconomyArray.push(currentBowlerArray);
    }

    // sorted in descending economy
    bowlersAndEconomyArray.sort((a, b) => a[1] - b[1]);

    let ans = [];

    // got top  economical bowlersData
    for (let i = 0; i < top; i++) {
        ans.push(bowlersAndEconomyArray[i][0]);
    }
    // console.log(ans);
    return ans;
}
module.exports = topEconomicalBowlers;
