
function strikeRateEachBatsmanPerYear(matchesArray, deliveriesArray) {
    // mapping id and year together
    let matchIdAndYear = {};
    let batsmanData = {};

    for (const match of matchesArray) {
        if (matchIdAndYear[match['id']] == undefined) {
            matchIdAndYear[match['id']] = "";
        }
        matchIdAndYear[match['id']] += (match["season"]);
    }


    for (const deliveries of deliveriesArray) {
        let matchId = deliveries["match_id"];
        let batsman = deliveries["batsman"];
        let matchSeason = matchIdAndYear[matchId];
        if (batsmanData[batsman] == undefined) {
            batsmanData[batsman] = {};
            batsmanData[batsman][matchSeason] = { "runs": 0, "balls": 0, "strikeRate": 0, };
        }
        else if ((batsmanData[batsman] !== undefined) && (batsmanData[batsman][matchSeason] == undefined)) {
            batsmanData[batsman][matchSeason] = { "runs": 0, "balls": 0, "strikeRate": 0, }
        }
        batsmanData[batsman][matchSeason]["runs"] += Number(deliveries["batsman_runs"]);
        batsmanData[batsman][matchSeason]["balls"] += 1;
        batsmanData[batsman][matchSeason]["strikeRate"] = ((batsmanData[deliveries["batsman"]][matchSeason]["runs"]) * 100) / (batsmanData[deliveries["batsman"]][matchIdAndYear[matchId]]["balls"]);
    }

    let finalAns = {};

    for (const batsman in batsmanData) {
        finalAns[batsman] = {};
        for (const year in batsmanData[batsman]) {
            finalAns[batsman][year] = {};
            finalAns[batsman][year] = batsmanData[batsman][year]["strikeRate"]
        }
    }

    // console.log(finalAns);
    return finalAns;
}


module.exports = strikeRateEachBatsmanPerYear;