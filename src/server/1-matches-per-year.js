
function pureMatchesPerYear(matchesArray) {
    let total = {};
    for (const match of matchesArray) {
        if (!total[match.season]) {
            total[match.season] = 1;
        }
        else {
            total[match.season] += 1;
        }
    }
    return total;
}

module.exports = pureMatchesPerYear;