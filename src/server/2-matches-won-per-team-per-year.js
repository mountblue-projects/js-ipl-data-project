function matchWonPerTeamPerYear(matchesArray) {
  let total = {};

  for (const match of matchesArray) {
    if (
      total[match.winner] !== undefined &&
      total[match.winner][match.season] == undefined
    ) {
      total[match.winner][match.season] = 1;
    } else if (total[match.winner] == undefined) {
      total[match.winner] = {};
      total[match.winner][match.season] = 1;
    } else {
      total[match.winner][match.season]++;
    }
  }
  return total;
}

module.exports = matchWonPerTeamPerYear;
