function highesPlayerDissmised(deliveriesArray) {
    let dismissalByBowlers = {};
    for (const deliveries of deliveriesArray) {
        if (deliveries["player_dismissed"] !== "") {
            let currentDeliveryBowler = deliveries["bowler"];
            let dismissedPlayer = deliveries["player_dismissed"];

            if (dismissalByBowlers[currentDeliveryBowler] == undefined) {
                dismissalByBowlers[currentDeliveryBowler] = {};
            }
            if (dismissalByBowlers[currentDeliveryBowler][dismissedPlayer] == undefined) {
                dismissalByBowlers[currentDeliveryBowler][dismissedPlayer] = 0;
            }

            dismissalByBowlers[currentDeliveryBowler][dismissedPlayer] += 1;
        }
    }
    // console.log(dismissalByBowlers);
    let highestDissmisal = 0;
    for (const bowler in dismissalByBowlers) {
        for (const playerDismissed in dismissalByBowlers[bowler]) {
            if (highestDissmisal < dismissalByBowlers[bowler][playerDismissed]) {
                highestDissmisal = dismissalByBowlers[bowler][playerDismissed];
            }
        }
    }
    // console.log(highestDissmisal);

    let ans = [];
    function current(bowler, dismised) {
        this.bowler = bowler;
        this.playerDismised = dismised;
        this.times = highestDissmisal;
    }
    for (const bowler in dismissalByBowlers) {
        for (const player_dismissed in dismissalByBowlers[bowler]) {
            if (dismissalByBowlers[bowler][player_dismissed] == highestDissmisal) {
                const newDismmisedPlayer = new current(bowler, player_dismissed)
                ans.push(newDismmisedPlayer);
            }
        }
    }
    return ans;
}

module.exports = highesPlayerDissmised;