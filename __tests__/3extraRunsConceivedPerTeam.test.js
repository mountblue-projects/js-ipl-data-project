const extraRunsConceivedPerTeam = require("../src/server/3-extra-runs-conceded-per-team");

const testFiles = require("../testFiles/3.js");


test("test 1 extra runs conceived per team", () => {
    expect(extraRunsConceivedPerTeam(testFiles["1"]["matchesArray"], testFiles["1"]["deliveriesArray"], testFiles["1"]["givenYear"])).toEqual(testFiles["1"]["testAns"]);
})
test("test 2 extra runs conceived per team", () => {
    expect(extraRunsConceivedPerTeam(testFiles["2"]["matchesArray"], testFiles["2"]["deliveriesArray"], testFiles["2"]["givenYear"])).toEqual(testFiles["2"]["testAns"]);
})
test("test 3 extra runs conceived per team", () => {
    expect(extraRunsConceivedPerTeam(testFiles["3"]["matchesArray"], testFiles["3"]["deliveriesArray"], testFiles["3"]["givenYear"])).toEqual(testFiles["3"]["testAns"]);
})
