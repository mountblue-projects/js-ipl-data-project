const strikeRateEachBatsmanPerYear = require("../src/server/7-strike-rate-each-batsman-per-season");

const testFiles = require("../testFiles/7.js");


test("test 1 strike Rate Each Batsman Per Year", () => {
    expect(strikeRateEachBatsmanPerYear(testFiles["1"]["matchesArray"], testFiles["1"]["deliveriesArray"])).toEqual(testFiles["1"]["testAns"]);
})
test("test 2 strike Rate Each Batsman Per Year", () => {
    expect(strikeRateEachBatsmanPerYear(testFiles["2"]["matchesArray"], testFiles["2"]["deliveriesArray"])).toEqual(testFiles["2"]["testAns"]);
})
test("test 3 strike Rate Each Batsman Per Year", () => {
    expect(strikeRateEachBatsmanPerYear(testFiles["3"]["matchesArray"], testFiles["3"]["deliveriesArray"])).toEqual(testFiles["3"]["testAns"]);
})
