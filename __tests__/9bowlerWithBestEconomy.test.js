const bowlerWithBestEconomy = require("../src/server/9-bowler-with-best-economy-super-over");

const testFiles = require("../testFiles/9.js");

test("test 1 bowler With Best Economy ", () => {
    expect((bowlerWithBestEconomy(testFiles["1"]["deliveriesArray"])).sort()).toEqual((testFiles["1"]["testAns"]).sort());
})
test("test 2 bowler With Best Economy ", () => {
    expect((bowlerWithBestEconomy(testFiles["2"]["deliveriesArray"])).sort()).toEqual((testFiles["2"]["testAns"]).sort());
})
test("test 3 bowler With Best Economy ", () => {
    expect((bowlerWithBestEconomy(testFiles["3"]["deliveriesArray"])).sort()).toEqual((testFiles["3"]["testAns"]).sort());
})  