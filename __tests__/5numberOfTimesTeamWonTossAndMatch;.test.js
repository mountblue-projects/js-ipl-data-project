const numberOfTimesTeamWonTossAndMatch = require("../src/server/5-number-of-times-each-team-won-toss-and-match");

const testFiles = require("../testFiles/5.js");


test("test 1 number Of Times Team Won Toss And Match", () => {
    expect(numberOfTimesTeamWonTossAndMatch(testFiles["1"]["matchesArray"])).toEqual(testFiles["1"]["testAns"]);
})
test("test 2 number Of Times Team Won Toss And Match", () => {
    expect(numberOfTimesTeamWonTossAndMatch(testFiles["2"]["matchesArray"])).toEqual(testFiles["2"]["testAns"]);
})
test("test 3 number Of Times Team Won Toss And Match", () => {
    expect(numberOfTimesTeamWonTossAndMatch(testFiles["3"]["matchesArray"])).toEqual(testFiles["3"]["testAns"]);
})
