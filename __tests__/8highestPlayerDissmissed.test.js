const highesPlayerDissmised = require("../src/server/8-highest-dismised.js");


const testFiles = require("../testFiles/8.js");

test("test 1 highest number of times one player has been dismissed by another player", () => {
    expect(highesPlayerDissmised(testFiles["1"]["deliveriesArray"])).toEqual(testFiles["1"]["testAns"]);
})
test("test 2 highest number of times one player has been dismissed by another player", () => {
    expect(highesPlayerDissmised(testFiles["2"]["deliveriesArray"])).toEqual(testFiles["2"]["testAns"]);
})
test("test 3 highest number of times one player has been dismissed by another player", () => {
    expect(highesPlayerDissmised(testFiles["3"]["deliveriesArray"])).toEqual(testFiles["3"]["testAns"]);
})