const pureMatchesPerYear = require("../src/server/1-matches-per-year.js");

// const pureMatchesPerYear = require("./src/server/1-matches-per-year.js");

const testfiles = require("../testFiles/1.js");


test("test 1 matches per year", () => {
    expect(pureMatchesPerYear(testfiles["1"]["testObject"])).toEqual(testfiles["1"]["testAns"]);
})

test("test 2 matches per year", () => {
    expect(pureMatchesPerYear(testfiles["2"]["testObject"])).toEqual(testfiles["2"]["testAns"]);
})

test("test 3 matches per year", () => {
    expect(pureMatchesPerYear(testfiles["3"]["testObject"])).toEqual(testfiles["3"]["testAns"]);
})