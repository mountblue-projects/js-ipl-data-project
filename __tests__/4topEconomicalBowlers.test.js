const topEconomicalBowlers = require("../src/server/4-top-10-economical-bowlers");

const testFiles = require("../testFiles/4.js");


test("test 1 top economical bowlers", () => {
    expect(topEconomicalBowlers(testFiles["1"]["matchesArray"], testFiles["1"]["deliveriesArray"], testFiles["1"]["givenYear"], testFiles["1"]["top"])).toEqual(testFiles["1"]["testAns"]);
})
test("test 2 top economical bowlers", () => {
    expect(topEconomicalBowlers(testFiles["2"]["matchesArray"], testFiles["2"]["deliveriesArray"], testFiles["2"]["givenYear"], testFiles["2"]["top"])).toEqual(testFiles["2"]["testAns"]);
})
test("test 3 top economical bowlers", () => {
    expect(topEconomicalBowlers(testFiles["3"]["matchesArray"], testFiles["3"]["deliveriesArray"], testFiles["3"]["givenYear"], testFiles["3"]["top"])).toEqual(testFiles["3"]["testAns"]);
})
