const pureMatchesPerYear = require("../src/server/2-matches-won-per-team-per-year.js");

const testfiles = require("../testFiles/2.js");


test("test 1 matches won per team per year", () => {
    expect(pureMatchesPerYear(testfiles["1"]["testObject"])).toEqual(testfiles["1"]["testAns"]);
})

test("test 2 matches won per team per year", () => {
    expect(pureMatchesPerYear(testfiles["2"]["testObject"])).toEqual(testfiles["2"]["testAns"]);
})

test("test 3 matches won per team per year", () => {
    expect(pureMatchesPerYear(testfiles["3"]["testObject"])).toEqual(testfiles["3"]["testAns"]);
})