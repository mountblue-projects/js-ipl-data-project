const highestPlayerPerYear = require("../src/server/6-highest-time-player-of-the-match-each-year");

const testfiles = require("../testFiles/6.js");


test("test 1  highest number of Player of the Match awards for each season", () => {
    expect(highestPlayerPerYear(testfiles["1"]["matchesArray"])).toEqual(testfiles["1"]["testAns"]);
})
test("test 2  highest number of Player of the Match awards for each season", () => {
    expect(highestPlayerPerYear(testfiles["2"]["matchesArray"])).toEqual(testfiles["2"]["testAns"]);
})
test("test 3  highest number of Player of the Match awards for each season", () => {
    expect(highestPlayerPerYear(testfiles["3"]["matchesArray"])).toEqual(testfiles["3"]["testAns"]);
})
