const csv = require('csv-parser');
const fs = require('fs');

const pureMatchesPerYear = require('./src/server/1-matches-per-year.js');
const matchWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.js');
const extraRunsConceivedPerTeam = require('./src/server/3-extra-runs-conceded-per-team.js');
const topEconomicalBowlers = require('./src/server/4-top-10-economical-bowlers.js');
const numberOfTimesTeamWonTossAndMatch = require('./src/server/5-number-of-times-each-team-won-toss-and-match.js');
const highestPlayerPerYear = require('./src/server/6-highest-time-player-of-the-match-each-year.js');
const strikeRateEachBatsmanPerYear = require('./src/server/7-strike-rate-each-batsman-per-season.js');
const highesPlayerDissmised = require('./src/server/8-highest-dismised.js');
const bowlerWithBestEconomy = require('./src/server/9-bowler-with-best-economy-super-over.js');

const matchesPath = './src/data/matches.csv';
const deliveriesPath = './src/data/deliveries.csv';

let matchesArray = [];
let deliveriesArray = [];

// pushes matches.csv conversions to matchesArray;
fs.createReadStream(matchesPath)
    .pipe(csv({}))
    .on('data', (data) => matchesArray.push(data))
    .on('end', () => {
        // pushes deliveries.csv conversions to deliveriesArray;
        fs.createReadStream(deliveriesPath)
            .pipe(csv({}))
            .on('data', (data) => deliveriesArray.push(data))
            .on('end', () => {
                // 1 - call pure matchesPerYear.
                try {
                    const ans = pureMatchesPerYear(matchesArray);

                    fs.writeFileSync(
                        './src/public/output/matchesPerYear.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '1 - created file matchesPerYear.json in ./src/public/output/',
                    );
                } catch (error) {
                    console.log('error in 1 matchesPerYear');
                    console.log(error);
                }

                // 2 - call matchesWonPerTeamPerYear.
                try {
                    const ans = matchWonPerTeamPerYear(matchesArray);

                    fs.writeFileSync(
                        './src/public/output/matchesWonPerTeamPerYear.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '2 - matchesWonPerTeamPerYear created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 2 matchWonPerTeamPerYear');
                    console.log(error);
                }

                // 3 - call extraRunsConceivedPerTeam.
                try {
                    const ans = extraRunsConceivedPerTeam(
                        matchesArray,
                        deliveriesArray,
                        '2016',
                    );

                    fs.writeFileSync(
                        './src/public/output/extraRunsConcevedPerTeam.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '3 - extraRunsConcevedPerTeam created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 3 extraRunsConceivedPerTeam');
                    console.log(error);
                }

                // 4 - call topEconomicalBowlers
                try {
                    const ans = topEconomicalBowlers(
                        matchesArray,
                        deliveriesArray,
                        '2015',
                        10,
                    );

                    fs.writeFileSync(
                        './src/public/output/top10EconomicalBowlersin2015.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '4 - top10EconomicalBowlersin2015 created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 4 topEconomicalBowlers');
                    console.log(error);
                }

                // 5 - call numberOfTimesTeamWonTossAndMatch
                try {
                    const ans = numberOfTimesTeamWonTossAndMatch(matchesArray);

                    fs.writeFileSync(
                        './src/public/output/numberOfTimesTeamWonTossAndMatch.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '5 - numberOfTimesTeamWonTossAndMatch created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 5 numberOfTimesTeamWonTossAndMatch');
                    console.log(error);
                }

                // 6 - call numberOfTimesTeamWonTossAndMatch
                try {
                    const ans = highestPlayerPerYear(matchesArray);

                    fs.writeFileSync(
                        './src/public/output/highestPlayerPerYear.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '6 - highestPlayerPerYear created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 6 highestPlayerPerYear');
                    console.log(error);
                }

                // 7 - call strikeRateEachBatsmanPerYear
                try {
                    const ans = strikeRateEachBatsmanPerYear(
                        matchesArray,
                        deliveriesArray,
                    );

                    fs.writeFileSync(
                        './src/public/output/strikeRateEachBatsmanPerYear.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '7 - strikeRateEachBatsmanPerYear created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 7 strikeRateEachBatsmanPerYear');
                    console.log(error);
                }

                // 8 - call highesPlayerDissmised
                try {
                    const ans = highesPlayerDissmised(deliveriesArray);

                    fs.writeFileSync(
                        './src/public/output/highesPlayerDissmised.json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '8 - highesPlayerDissmised created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 8 highesPlayerDissmised');
                    console.log(error);
                }

                // 9 - call bowlerWithBestEconomy

                try {
                    const ans = bowlerWithBestEconomy(deliveriesArray);

                    fs.writeFileSync(
                        './src/public/output/bowlerWithBestEconomy .json',
                        JSON.stringify(ans, null, 2),
                    );
                    console.log(
                        '9 - bowlerWithBestEconomy  created in directory : src/public/output/ ',
                    );
                } catch (error) {
                    console.log('error in 9 bowlerWithBestEconomy ');
                    console.log(error);
                }
            });
    });
