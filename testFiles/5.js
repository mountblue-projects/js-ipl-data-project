/* eslint-disable no-undef */

testFile = {
    "1": {
        matchesArray: [
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Sunrisers Hyderabad' },
            { id: '635', toss_winner: 'Rising Pune Supergiants', winner: 'Royal Challengers Bangalore' },
            { id: '635', toss_winner: 'Kolkata Knight Riders', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Kolkata Knight Riders', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Kings XI Punjab', winner: 'Kings XI Punjab' },
            { id: '635', toss_winner: 'Delhi Daredevils', winner: 'Sunrisers Hyderabad' },
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Sunrisers Hyderabad' },
        ],
        testAns: {
            'Sunrisers Hyderabad': 2,
            'Kolkata Knight Riders': 2,
            'Kings XI Punjab': 1
        }
    },
    "2": {
        matchesArray: [
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Royal Challengers Bangalore' },
            { id: '635', toss_winner: 'Royal Challengers Bangalore', winner: 'Royal Challengers Bangalore' },
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Kolkata Knight Riders', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Kings XI Punjab' },
            { id: '635', toss_winner: 'Delhi Daredevils', winner: 'Delhi Daredevils' },
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Sunrisers Hyderabad' },
        ],
        testAns: {
            'Royal Challengers Bangalore': 1,
            'Kolkata Knight Riders': 1,
            'Delhi Daredevils': 1,
            'Sunrisers Hyderabad': 1
        }

    },
    "3": {
        matchesArray: [
            { id: '635', toss_winner: 'Royal Challengers Bangalore', winner: 'Sunrisers Hyderabad' },
            { id: '635', toss_winner: 'Rising Pune Supergiants', winner: 'Rising Pune Supergiants' },
            { id: '635', toss_winner: 'Kolkata Knight Riders', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Kolkata Knight Riders', winner: 'Kolkata Knight Riders' },
            { id: '635', toss_winner: 'Delhi Daredevils', winner: 'Kings XI Punjab' },
            { id: '635', toss_winner: 'Delhi Daredevils', winner: 'Delhi Daredevils' },
            { id: '635', toss_winner: 'Sunrisers Hyderabad', winner: 'Kolkata Knight Riders' },
        ],
        testAns: {
            'Rising Pune Supergiants': 1,
            'Kolkata Knight Riders': 2,
            'Delhi Daredevils': 1
        }
    },
}

module.exports = testFile;