/* eslint-disable no-undef */
const testObject1 = [{
  id: '636',
  season: '2016',
},
{
  id: '633',
  season: '2016',

},
{
  id: '567',
  season: '2015',
},
{
  id: '568',
  season: '2015',
},
{
  id: '451',
  season: '2013',
},
{
  id: '452',
  season: '2013',
},
{
  id: '373',
  season: '2012',
},
{
  id: '374',
  season: '2012',
},
{
  id: '343',
  season: '2012',
},
{
  id: '344',
  season: '2012',
},
{
  id: '331',
  season: '2012',
},
{
  id: '332',
  season: '2012',
},
{
  id: '243',
  season: '2011',
},
{
  id: '244',
  season: '2011',
},

{
  id: '234',
  season: '2010',
},
{
  id: '235',
  season: '2011',
}
]


const testAns1 = { '2010': 1, '2011': 3, '2012': 6, '2013': 2, '2015': 2, '2016': 2 }


const testObject2 = [{
  id: '636',
  season: '2016',
},
{
  id: '633',
  season: '2016',

},
{
  id: '374',
  season: '2012',
},
{
  id: '343',
  season: '2012',
},
{
  id: '344',
  season: '2012',
},
{
  id: '331',
  season: '2012',
},
{
  id: '235',
  season: '2011',
}
]

const testAns2 = { '2011': 1, '2012': 4, '2016': 2 };

const testObject3 = [{
  id: '636',
  season: '2016',
},
{
  id: '374',
  season: '2012',
},
{
  id: '343',
  season: '2012',
},
{
  id: '331',
  season: '2012',
},
]

testAns3 = { '2012': 3, '2016': 1 };



module.exports = { "1": { testObject: testObject1, testAns: testAns1 }, "2": { testObject: testObject2, testAns: testAns2 }, "3": { testObject: testObject3, testAns: testAns3 } };
// module.exports = testObject3;