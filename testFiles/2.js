/* eslint-disable no-undef */

const testObject1 = [{
  id: '636',
  season: '2016',
  winner: 'Sunrisers Hyderabad',
},
{
  id: '633',
  season: '2016',
  winner: 'Royal Challengers Bangalore',

},
{
  id: '567',
  season: '2015',
  winner: 'Mumbai Indians',
},
{
  id: '568',
  season: '2015',
  winner: 'Royal Challengers Bangalore',
},
{
  id: '451',
  season: '2013',
  winner: 'Pune Warriors',
},
{
  id: '452',
  season: '2013',
  winner: 'Royal Challengers Bangalore',
},
{
  id: '373',
  season: '2012',
  winner: 'Deccan Chargers',
},
{
  id: '374',
  season: '2012',
  winner: 'Delhi Daredevils',
},
{
  id: '343',
  season: '2012',
  winner: 'Kings XI Punjab',
},
{
  id: '344',
  season: '2012',
  winner: 'Kolkata Knight Riders',
},
{
  id: '331',
  season: '2012',
  winner: 'Delhi Daredevils',
},
{
  id: '332',
  season: '2012',
  winner: 'Chennai Super Kings',
},
{
  id: '243',
  season: '2011',
  winner: 'Kings XI Punjab',
},
{
  id: '244',
  season: '2011',
  winner: 'Pune Warriors',
},

{
  id: '234',
  season: '2010',
  winner: 'Chennai Super Kings',
},
{
  id: '235',
  season: '2011',
  winner: 'Chennai Super Kings',
}
]


const testAns1 = {
  'Sunrisers Hyderabad': { '2016': 1 },
  'Royal Challengers Bangalore': { '2013': 1, '2015': 1, '2016': 1 },
  'Mumbai Indians': { '2015': 1 },
  'Pune Warriors': { '2011': 1, '2013': 1 },
  'Deccan Chargers': { '2012': 1 },
  'Delhi Daredevils': { '2012': 2 },
  'Kings XI Punjab': { '2011': 1, '2012': 1 },
  'Kolkata Knight Riders': { '2012': 1 },
  'Chennai Super Kings': { '2010': 1, '2011': 1, '2012': 1 }
}


const testObject2 = [{
  id: '636',
  season: '2016',
  winner: 'Sunrisers Hyderabad',
},
{
  id: '633',
  season: '2016',
  winner: 'Royal Challengers Bangalore',

},
{
  id: '374',
  season: '2012',
  winner: 'Delhi Daredevils',
},
{
  id: '343',
  season: '2012',
  winner: 'Kings XI Punjab',
},
{
  id: '344',
  season: '2012',
  winner: 'Kolkata Knight Riders',
},
{
  id: '331',
  season: '2012',
  winner: 'Delhi Daredevils',
},
{
  id: '235',
  season: '2011',
  winner: 'Chennai Super Kings',
}
];

const testAns2 = {
  'Sunrisers Hyderabad': { '2016': 1 },
  'Royal Challengers Bangalore': { '2016': 1 },
  'Delhi Daredevils': { '2012': 2 },
  'Kings XI Punjab': { '2012': 1 },
  'Kolkata Knight Riders': { '2012': 1 },
  'Chennai Super Kings': { '2011': 1 }
};

const testObject3 = [{
  id: '636',
  season: '2016',
  winner: 'Sunrisers Hyderabad',
},
{
  id: '374',
  season: '2012',
  winner: 'Delhi Daredevils',
},
{
  id: '343',
  season: '2012',
  winner: 'Kings XI Punjab',
},
{
  id: '331',
  season: '2012',
  winner: 'Delhi Daredevils',
},
]

testAns3 = {
  'Sunrisers Hyderabad': { '2016': 1 },
  'Delhi Daredevils': { '2012': 2 },
  'Kings XI Punjab': { '2012': 1 }
};



module.exports = { "1": { testObject: testObject1, testAns: testAns1 }, "2": { testObject: testObject2, testAns: testAns2 }, "3": { testObject: testObject3, testAns: testAns3 } };
// module.exports = testObject3;