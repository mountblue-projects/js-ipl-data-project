/* eslint-disable no-undef */

testFiles = {
    "1": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', total_runs: '6' },
            { match_id: '45', bowler: 'M de Lange', total_runs: '0' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '3' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '4' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '5' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '4' },
            { match_id: '65', bowler: 'M de Lange', total_runs: '1' },
            { match_id: '65', bowler: 'R Ashwin', total_runs: '0' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '2' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '0' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '0' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '1' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '3' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '2' },
            { match_id: '42', bowler: 'Z Khan', total_runs: '6' },
            { match_id: '42', bowler: 'Parvez Rasool', total_runs: '4' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '5' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '3' },
            { match_id: '36', bowler: 'V Kohli', total_runs: '6' },
            { match_id: '36', bowler: 'S Nadeem', total_runs: '1' },
            { match_id: '10', bowler: 'M de Lange', total_runs: '2' },
            { match_id: '10', bowler: 'Z Khan', total_runs: '3' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '2' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '5' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '2' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '2' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '1' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '0' },
        ],
        testAns: ['R Ashwin']
    },
    "2": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', total_runs: '0' },
            { match_id: '45', bowler: 'M de Lange', total_runs: '0' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '3' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '4' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '5' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '4' },
            { match_id: '65', bowler: 'M de Lange', total_runs: '1' },
            { match_id: '65', bowler: 'R Ashwin', total_runs: '5' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '1' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '0' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '0' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '6' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '4' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '0' },
            { match_id: '42', bowler: 'Z Khan', total_runs: '1' },
            { match_id: '42', bowler: 'Parvez Rasool', total_runs: '5' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '5' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '3' },
            { match_id: '36', bowler: 'V Kohli', total_runs: '6' },
            { match_id: '36', bowler: 'S Nadeem', total_runs: '1' },
            { match_id: '10', bowler: 'M de Lange', total_runs: '2' },
            { match_id: '10', bowler: 'Z Khan', total_runs: '4' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '2' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '3' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '2' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '2' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '1' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '0' },
        ],
        testAns: ['M de Lange']

    },
    "3": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', total_runs: '3' },
            { match_id: '45', bowler: 'M de Lange', total_runs: '3' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '3' },
            { match_id: '45', bowler: 'Parvez Rasool', total_runs: '3' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '3' },
            { match_id: '65', bowler: 'Z Khan', total_runs: '3' },
            { match_id: '65', bowler: 'M de Lange', total_runs: '3' },
            { match_id: '65', bowler: 'R Ashwin', total_runs: '3' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '3' },
            { match_id: '89', bowler: 'S Nadeem', total_runs: '3' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '3' },
            { match_id: '89', bowler: 'J Yadav', total_runs: '3' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '3' },
            { match_id: '42', bowler: 'V Kohli', total_runs: '3' },
            { match_id: '42', bowler: 'Z Khan', total_runs: '3' },
            { match_id: '42', bowler: 'Parvez Rasool', total_runs: '3' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '3' },
            { match_id: '36', bowler: 'B Kumar', total_runs: '3' },
            { match_id: '36', bowler: 'V Kohli', total_runs: '3' },
            { match_id: '36', bowler: 'S Nadeem', total_runs: '3' },
            { match_id: '10', bowler: 'M de Lange', total_runs: '3' },
            { match_id: '10', bowler: 'Z Khan', total_runs: '3' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '3' },
            { match_id: '10', bowler: 'Mustafizur Rahman', total_runs: '3' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '3' },
            { match_id: '9', bowler: 'S Nadeem', total_runs: '3' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '3' },
            { match_id: '9', bowler: 'M de Lange', total_runs: '3' },
        ],
        testAns: [
            'J Yadav',
            'M de Lange',
            'Parvez Rasool',
            'Z Khan',
            'R Ashwin',
            'V Kohli',
            'B Kumar',
            'Mustafizur Rahman',
            'S Nadeem',
        ]


    },
}

module.exports = testFiles;