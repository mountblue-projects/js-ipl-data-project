/* eslint-disable no-undef */

testFile = {
    "1": {
        matchesArray: [
            { id: '635', season: '2016', player_of_match: 'BCJ Cutting' },
            { id: '635', season: '2016', player_of_match: 'M Erasmus' },
            { id: '635', season: '2016', player_of_match: 'BCJ Cutting' },
            { id: '635', season: '2016', player_of_match: 'DR Smith' },
            { id: '635', season: '2015', player_of_match: 'SK Raina' },
            { id: '635', season: '2015', player_of_match: 'DR Smith' },
            { id: '635', season: '2015', player_of_match: 'UT Yadav' },
            { id: '635', season: '2013', player_of_match: 'LMP Simmons' },
            { id: '635', season: '2013', player_of_match: 'LMP Simmons' },
        ],
        testAns: {
            "2016": { 'BCJ Cutting': 2 },
            "2015": {
                'SK Raina': 1,
                'DR Smith': 1,
                'UT Yadav': 1
            },
            "2013": { 'LMP Simmons': 2 }
        }
    },
    "2": {
        matchesArray: [
            { id: '635', season: '2013', player_of_match: 'M Erasmus' },
            { id: '635', season: '2013', player_of_match: 'M Erasmus' },
            { id: '635', season: '2012', player_of_match: 'BCJ Cutting' },
            { id: '635', season: '2012', player_of_match: 'DR Smith' },
            { id: '635', season: '2012', player_of_match: 'SK Raina' },
            { id: '635', season: '2012', player_of_match: 'DR Smith' },
            { id: '635', season: '2012', player_of_match: 'UT Yadav' },
            { id: '635', season: '2012', player_of_match: 'LMP Simmons' },
            { id: '635', season: '2012', player_of_match: 'LMP Simmons' },
        ],
        testAns: {
            "2013": { 'M Erasmus': 2 },
            "2012": {
                'DR Smith': 2,
                'LMP Simmons': 2
            }
        }
    },
    "3": {
        matchesArray: [
            { id: '635', season: '2016', player_of_match: 'BCJ Cutting' },
            { id: '635', season: '2015', player_of_match: 'M Erasmus' },
            { id: '635', season: '2016', player_of_match: 'BCJ Cutting' },
            { id: '635', season: '2016', player_of_match: 'DR Smith' },
            { id: '635', season: '2015', player_of_match: 'SK Raina' },
            { id: '635', season: '2017', player_of_match: 'DR Smith' },
            { id: '635', season: '2015', player_of_match: 'UT Yadav' },
            { id: '635', season: '2020', player_of_match: 'LMP Simmons' },
            { id: '635', season: '2020', player_of_match: 'LMP Simmons' },
        ],
        testAns: {
            "2016": { 'BCJ Cutting': 2 },
            "2015": {
                'SK Raina': 1,
                'M Erasmus': 1,
                'UT Yadav': 1
            },
            "2017": { 'DR Smith': 1 },
            "2020": { 'LMP Simmons': 2 }
        }
    },
}

module.exports = testFile;