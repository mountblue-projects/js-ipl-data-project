/* eslint-disable no-undef */

testFile = {
    "1": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', player_dismissed: 'AN Ahmed' },
            { match_id: '45', bowler: 'M de Lange', player_dismissed: '' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'DT Christian' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'S Sohal' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'B Chipli' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'JR Hopes' },
            { match_id: '65', bowler: 'M de Lange', player_dismissed: 'NV Ojha' },
            { match_id: '65', bowler: 'R Ashwin', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'DT Christian' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'RV Uthappa' },
            { match_id: '89', bowler: 'J Yadav', player_dismissed: 'MD Mishra' },
            { match_id: '89', bowler: 'J Yadav', player_dismissed: 'MD Mishra' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'IK Pathan' },
            { match_id: '42', bowler: 'Z Khan', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'Parvez Rasool', player_dismissed: 'V Sehwag' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'S Dhawan' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'V Kohli', player_dismissed: 'DA Warner' },
            { match_id: '36', bowler: 'S Nadeem', player_dismissed: 'V Sehwag' },
            { match_id: '10', bowler: 'Harmeet Singh', player_dismissed: 'S Sohal' },
            { match_id: '10', bowler: 'Z Khan', player_dismissed: 'DA Warner' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'S Dhawan' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'DT Christian' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'DT Christian' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'IK Pathan' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'AN Ahmed' },
        ],
        testAns: [
            {
                bowler: 'S Nadeem',
                playerDismised: 'DT Christian',
                times: 3
            }
        ]

    },
    "2": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', player_dismissed: 'MD Mishra' },
            { match_id: '45', bowler: 'M de Lange', player_dismissed: '' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'MD Mishra' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'MD Mishra' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'B Chipli' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'JR Hopes' },
            { match_id: '65', bowler: 'M de Lange', player_dismissed: 'NV Ojha' },
            { match_id: '65', bowler: 'R Ashwin', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'DT Christian' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'RV Uthappa' },
            { match_id: '89', bowler: 'J Yadav', player_dismissed: 'MD Mishra' },
            { match_id: '89', bowler: 'J Yadav', player_dismissed: 'MD Mishra' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'IK Pathan' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'IK Pathan' },
            { match_id: '42', bowler: 'Z Khan', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'Parvez Rasool', player_dismissed: 'MD Mishra' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'S Dhawan' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'V Kohli', player_dismissed: 'DA Warner' },
            { match_id: '36', bowler: 'S Nadeem', player_dismissed: 'V Sehwag' },
            { match_id: '10', bowler: 'M de Lange', player_dismissed: 'S Sohal' },
            { match_id: '10', bowler: 'Z Khan', player_dismissed: 'DA Warner' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'S Dhawan' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'DA Warner' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'DT Christian' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'DA Warner' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'AN Ahmed' },
        ],
        testAns: [
            { bowler: 'J Yadav', playerDismised: 'MD Mishra', times: 3 },
            {
                bowler: 'Parvez Rasool',
                playerDismised: 'MD Mishra',
                times: 3
            }
        ]


    },
    "3": {
        deliveriesArray: [
            { match_id: '45', bowler: 'J Yadav', player_dismissed: 'NV Ojha' },
            { match_id: '45', bowler: 'M de Lange', player_dismissed: 'DT Christian' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'AN Ahmed' },
            { match_id: '45', bowler: 'Parvez Rasool', player_dismissed: 'AN Ahmed' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'DA Warner' },
            { match_id: '65', bowler: 'Z Khan', player_dismissed: 'AN Ahmed' },
            { match_id: '65', bowler: 'M de Lange', player_dismissed: 'RV Uthappa' },
            { match_id: '65', bowler: 'R Ashwin', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'S Nadeem', player_dismissed: 'AN Ahmed' },
            { match_id: '89', bowler: 'J Yadav', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'V Kohli', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'Z Khan', player_dismissed: 'AN Ahmed' },
            { match_id: '42', bowler: 'Parvez Rasool', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'B Kumar', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'V Kohli', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'M de Lange', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'Z Khan', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'AN Ahmed' },
            { match_id: '10', bowler: 'Mustafizur Rahman', player_dismissed: 'AN Ahmed' },
            { match_id: '36', bowler: 'S Nadeem', player_dismissed: 'RV Uthappa' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'RV Uthappa' },
            { match_id: '9', bowler: 'S Nadeem', player_dismissed: 'RV Uthappa' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'AN Ahmed' },
            { match_id: '9', bowler: 'M de Lange', player_dismissed: 'AN Ahmed' },
        ],
        testAns: [
            {
                bowler: 'M de Lange',
                playerDismised: 'AN Ahmed',
                times: 3
            },
            {
                bowler: 'Parvez Rasool',
                playerDismised: 'AN Ahmed',
                times: 3
            },
            {
                bowler: 'Z Khan',
                playerDismised: 'AN Ahmed',
                times: 3
            },
            {
                bowler: 'S Nadeem',
                playerDismised: 'AN Ahmed',
                times: 3
            },
            {
                bowler: 'S Nadeem',
                playerDismised: 'RV Uthappa',
                times: 3
            },
            {
                bowler: 'V Kohli',
                playerDismised: 'AN Ahmed',
                times: 3
            }
        ]


    },
}

module.exports = testFile;